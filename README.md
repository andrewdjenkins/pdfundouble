# pdfundouble

Do you have a PDF with two adjacent document pages side by side on each PDF-page? Do you wish it had one document-page per PDF-page?

This script chains the free programs pdfcrop, pdfseparate, and ghostscript to make it happen.

## Usage

./pdfundouble INPUTFILE OUTPUTFILE
